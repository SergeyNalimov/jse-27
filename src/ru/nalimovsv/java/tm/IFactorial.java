package tm;

import java.math.BigInteger;

public interface IFactorial {
    BigInteger getFactorial(Integer n);
}
